#include <stdio.h>

typedef struct
{
	unsigned long TimeStamp;
	unsigned long Period;
	unsigned long BlockSize;
	unsigned long StartAddr;
	unsigned long EndAddr;
	unsigned long PageSize;
	unsigned long SaveAddr;
}BlockInfoT;

bool IsBlobkOK(unsigned long addr, unsigned long size)
{
	// if all block content is ff then return true
	return false;
}

void ErasePage(unsigned long addr)
{
	//erase ...
}

unsigned long GetBlockSaveAddr(BlockInfoT* pBlk)
{
	unsigned long tcnt = (pBlk->EndAddr - pBlk->StartAddr) / pBlk->BlockSize;
	unsigned long indx = pBlk->TimeStamp / pBlk->Period;
	indx %= tcnt;
	pBlk->SaveAddr = indx * pBlk->BlockSize + pBlk->StartAddr;

	if( (pBlk->SaveAddr / pBlk->PageSize) != ((pBlk->SaveAddr + pBlk->BlockSize - 1) / pBlk->PageSize) )
	{
		unsigned long part1 = (pBlk->SaveAddr / pBlk->PageSize + 1) * pBlk->PageSize - pBlk->SaveAddr;
		unsigned long part2 = pBlk->BlockSize - part1;
		if( IsBlobkOK(pBlk->SaveAddr,part1) == false ) ErasePage( (pBlk->SaveAddr / pBlk->PageSize) * pBlk->PageSize );
		if( IsBlobkOK(pBlk->SaveAddr+part1,part2) == false ) ErasePage( (pBlk->SaveAddr / pBlk->PageSize + 1) * pBlk->PageSize );
	}
	else if( IsBlobkOK(pBlk->SaveAddr,pBlk->BlockSize) == false ) ErasePage( (pBlk->SaveAddr / pBlk->PageSize) * pBlk->PageSize );

	return pBlk->SaveAddr;
} 

int main(void)
{
	printf( "This is a test program only.\r\n" );
	
	return 0;
}
